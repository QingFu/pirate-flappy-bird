/**
 * 数据库操作
 * @type {[type]}
 */
const Mysql = require('mysql');

module.exports = {
  /**
   * 初始化函数
   * @param  {[type]} options [description]
   * @return {[type]}         [description]
   */
  init: function (options) {
    return Mysql.createConnection(options)
  },
  /**
   * 连接数据
   * @param  {[type]} option [description]
   * @return {[type]}        [description]
   */
  connect: function (options) {
    let connection = this.init(options)
    connection.connect((err) => {
      if (err) {
        console.log('[connect error] - :' + err)
        return
      }
      console.log('[connection connect] succeed!')
    })
  },
  /**
   * 在数据库中插入数据的方法
   * @param  {[type]} sql    [description]
   * @param  {[type]} params [description]
   * @return {[type]}        [description]
   */
  insert: function (options, sql, params) {
    let connection = this.init(options)
    connection.query(sql, params, (err, result) => {
      if (err) {
        console.log('[INSERT ERROR] - ', err.message)
        return
      }
      console.log('[INSERT SUCCEED] - ', result)
    })
  },
  /**
   * 在数据库中更新数据的方法
   * @param  {[type]} sql    [description]
   * @param  {[type]} params [description]
   * @return {[type]}        [description]
   */
  update: function (options, sql, params) {
    let connection = this.init(options)
    connection.query(sql, params, (err, result) => {
      if (err) {
        console.log('[UPDATE ERROR] - ', err.message)
        return
      }
      console.log('[UPDATE SUCCEED] - ', result)
    })
  },
  /**
   * 在数据库中查询数据的方法
   * @param  {[type]} sql sql语句
   * @return {[type]}     [description]
   */
  select: (options, sql) => {
    let connection = Mysql.createConnection(options)
    connection.query(sql, (err, result) => {
      if (err) {
        console.log('[SELECT ERROR] - ', err.message)
        return false
      }
      console.log('SELECT affectedRows:', result)
      return result
    })
  },
  /**
   * 在数据库中删除数据的方法
   * @param  {[type]} sql sql语句
   * @return {[type]}     [description]
   */
  delete: (options, sql) => {
    let connection = this.init(options)
    connection.query(sql, (err, result) => {
      if (err) {
        console.log('[DELETE ERROR] - ', err.message)
        return
      }
      console.log('DELETE affectedRows:', result)
    })
  },
  /**
   * 断开数据库连接的方法
   * @param  {[type]} options [description]
   * @return {[type]}         [description]
   */
  disConnect: function (options) {
    let connection = this.init(options)
    connection.end((err) => {
      if (err) {
        console.log('[ERROR DISCONNECTION] - :' + err)
        return
      }
      console.log('[CONNECTION END]!')
    })
  }
}

/**
 * 数据库操作基本示例
 */
// 插入数据
// const sql = 'INSERT INTO userinfo(Id, UserName, UserPass) VALUES(0, ?, ?)';
// const params = ['测试', '2465'];
// db_insert_update(sql, params);

// 修改数据
// const sql = 'UPDATE userinfo SET UserName = ?,UserPass = ? WHERE Id = ?';
// const params = ['轻浮2', '9876', 2];
// db_insert_update(sql, params);

// 查询数据
// const sql = 'SELECT * FROM userinfo';
// db_query_delete(sql);

// 删除数据
// const sql = 'DELETE FROM userinfo';
// db_query_delete(sql);
