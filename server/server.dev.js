const commonConfig = require('../config/common.js')
const port = commonConfig.devPort
const Koa = require('koa')
const logger = require('koa-logger')
const webpack = require('webpack')

const serverRender = require('./middleware/ServerRender.js')
const webpackConfig = require('../webpack/webpack.dev.config')
const compilers = webpack(webpackConfig)

/**
 * 查找compiler
 */
function findCompiler (compilers, name) {
  return compilers.compilers.find(compiler => compiler.name === name)
}

// 创建koa实例
const app = new Koa()
app.use(logger())

serverRender(app, compilers)
.then((render) => {
  app.use(render)
  app.listen(port, (err) => {
    console.log(`\n Open http://localhost:${port}/ in your web browser.\n`)
  })
})
.catch((err) => {
  console.log('listen error :', err)
})

// 连接数据库
const DB = require('./server-db.js');
const option = {
  host: 'localhost',
  database: 'nodesample',
  user: 'root',
  password: 'wqf123',
  prot: '3306'
}

// 定义接口
const route = require('koa-route');

const login = ctx => {
  // 连接数据库
  DB.connect(option)
  ctx.response.type = 'json'
  console.log(ctx.req._parsedUrl.query)
  let sql = 'SELECT * FROM userinfo WHERE UserName = 12'
  let result = DB.select(option, sql)
  let data = {}
  // 如果查询到数据
  if (result) {
    console.log(123)
    data = {
      code: 200,
      data: result,
      msg: '登录成功'
    }
  }
  else {
    data = {
      code: 0,
      msg: '用户不存在'
    }
  }
  ctx.response.body = data
  // 断开数据库连接
  // DB.disConnect(option)
  return ctx.response
};
app.use(route.post('/login', login));