import React from 'react'
import axios from 'axios'
import './style.less'

export default class Bird extends React.Component {
  constructor(props, context) {
    super(props)
    this.state = {
      duration: 100,
      index: 0,
      imgUrl: require('../../assets/flappybird/bird0_1.png')
    }
  }
  /**
   * 跳跃的方法
   */
  Jump () {
    let date = {
      name: 12,
      psd: 123
    }
    /**
     * 请求的方法
     * @param  {[type]} '/login' [description]
     * @param  {[type]} {data:  123}).then((res) [description]
     * @return {[type]}          [description]
     */
    axios.post('/login', date).then((res) => {
      console.log(res.data)
    }).catch((err) => {
      console.log('ERROR - ', err)
    })
  }
  /**
   * 设置鸟的动画
   * @param {[type]} duration [description]
   */
  setBird (duration) {
    let Index = (this.state.index + 1) % 3
    this.timer = setTimeout(() => {
      let url = require('../../assets/flappybird/bird0_' + Index + '.png')
      this.setState({
        index: Index,
        imgUrl: url
      })
      // this.setBird(this.state.duration)
    }, duration)
  }
  componentDidMount() {
    this.setBird(this.state.duration)
  }
  componentWillUnmount () {
    this.timer && clearInterval(this.timer)
  }
  render() {
    return (
      <div className="bird-page" onClick={this.Jump}>
        <img src={this.state.imgUrl} />
      </div>
    )
  }
}
