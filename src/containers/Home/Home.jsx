import React from 'react'
import './style.less'
import { SetStaticContext, Bird } from 'Components'

export default class Home extends React.Component {
  render() {
    const seoInfo = {
      title: '盗版 flappy bird',
      keywords: '盗版 flappy bird',
      description: '盗版 flappy bird'
    }
    return (
      <SetStaticContext code={200} seoInfo={seoInfo}>
        <div className="home-page">
          <Bird></Bird>
        </div>
      </SetStaticContext>
    )
  }
}
