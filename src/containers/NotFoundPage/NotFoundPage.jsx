import React from 'react'
import { SetStaticContext } from 'Components'

export default class NotFoundPageContainer extends React.Component {
  render() {
    return (
      <SetStaticContext code={404}>
        <h1>
          您的页面不见了
        </h1>
      </SetStaticContext>
    )
  }
}
