/** 页面组件出口 **/
// 项目公共样式
import './style.less'

// 导入页面组件
import Home from './Home/Home.jsx'
import NotFoundPage from './NotFoundPage/NotFoundPage.jsx'

export {
  Home,
  NotFoundPage
}
